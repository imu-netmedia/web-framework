# 软件开发与管理课程 - Java EE

## 基于Maven的JavaSE项目

#### 介绍
在IDEA中创建基于Maven的JavaSE或Web项目，实现任意功能，并完成以下Maven功能的验证：

* 添加JUnit（版本不限）依赖，指定Scope。
* 运行Maven测试
* 部署运行项目
* 使用Javadoc插件生成项目文档


## 基于Model2的登录功能 

#### 介绍
基于Model2的登录功能
包括：
* 页面：Login.jsp，LoginSuccess(main).jsp
* Java类：LoginServlet.java，User.java
* 数据库：user表

## 基于Filter的用户登录验证

#### 介绍
- 类：LoginFilter.java
- 要求：退出系统后不能访问main.jsp，跳转到login.jsp
- 方法：点击退出，清除session

更复杂的验证例子，参考
http://www.cnblogs.com/coderland/p/5902878.html


## 基于Listener完成在线人数和登陆用户统计

#### 介绍
- 要求：显示在线人数和登陆用户数
- 方法：
1. 使用监听器（HttpSessionListener）完成功能
2. 加入用户退出功能
